from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.


# Функція яка виводить текст на екран
def index(request):
    return HttpResponse('''<p>Hello World!</p>
<p>Django is one of the best framework on Python</p>
<hr>''')


# Функція яка виводить текст на екран
def hello(request):
    return HttpResponse('Hello World')
