from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index')  # перенаправляє на функцію index, яка виводить текст на екран
]
