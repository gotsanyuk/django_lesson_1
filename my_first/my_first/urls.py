"""my_first URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from lesson_11.views import hello

urlpatterns = [
    path('admin/', admin.site.urls),
    path('lesson_1/', hello),  # при переході на lesson_1, відкриває ф-цію яка виводить 'hello world'
    path('lesson_1_1/', include('lesson_11.urls'))  # перенаправляє на urls з додатка lesson_11
]
